<nav>
  <ul class="pagination">
    <?php if( $totalPages > 0): ?>
      <li class="<?php if( $current <= 0): ?> disabled <? endif; ?>">
        <a id="prevBtn" href="#" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
        </a>
      </li>
    <?php endif; ?>
    <?php for($i = 0; $i <= $totalPages-1; $i++): ?>
      <li class="<?php if($i == $currentPage) echo 'active'; ?>"><a href="" class="page"><?php echo $i+1 ?></a></li>
    <?php endfor; ?>
    <?php if( $totalPages > 0): ?>
      <li class="<?php if( ( $currentPage + 1 ) == $totalPages): ?> disabled <? endif; ?>">
        <a id="nextBtn" href="#" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
        </a>
      </li>
    <?php endif; ?>
  </ul>
</nav>

<?php if( $totalPages > 0): ?>
  <div class="page-cnt">Page: <span class="current-page">
    <?php echo $currentPage+1; ?></span> of <?php echo $totalPages; ?>
  </div>
<?php endif; ?>
