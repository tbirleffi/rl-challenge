<?php if( $totalPages > 0): ?>
  <div class="table-responsive">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Rating:</th>
          <th>Date:</th>
          <th>Customer:</th>
          <th>Review:</th>
          <th>Source:</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($reviews as $review): ?>
          <tr>
            <th scope="row"><?php echo $review->rating; ?> of 5</th>
            <td>
              <?php
                $date = $review->date_of_submission;
                $date = date("m/d/Y", strtotime($date));
                echo $date;
              ?>
            </td>
            <td><a href="<?php echo $review->customer_url; ?>"><?php echo $review->customer_name; ?></a></td>
            <td class="desc"><?php echo $review->description; ?></td>
            <td>
              <?php
                if($review->review_from == 0) {
                  echo 'internal';
                } else {
                  echo ('<a href="' . $review->review_url . '">' . $review->review_source . '</a>');
                }
              ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <p>Sorry, there were no reviews found...</p>
<?php endif; ?>
