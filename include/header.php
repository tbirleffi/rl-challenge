<div class="page-header">
  <h1><?php echo $businessInfo->business_name; ?></h1>
  <address><?php echo $businessInfo->business_address; ?></address>
  <div class="details">
    <ul>
      <li><strong>Phone:</strong> <a href="tel:+1<?php echo $businessInfo->business_phone; ?>"><?php echo $businessInfo->business_phone; ?></a></li>
      <li><strong>Rating:</strong> <?php echo round($businessInfo->total_rating->total_avg_rating); ?> of 5</li>
      <li><strong>Total Reviews:</strong> <?php echo $totalReviews; ?></li>
    <ul>
  </div>
</div>
<div class="btns">
  <a href="<?php echo $businessInfo->external_page_url; ?>" class="btn btn-info" role="button">Website</a>
  <a href="<?php echo $businessInfo->external_url; ?>" class="btn btn-info right" role="button">Rate This Company</a>
</div>
