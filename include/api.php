<?php
// Code Challenge: RL
// Author: Tony Birleffi
// Date: 10-03-2015

$apiKey = '61067f81f8cf7e4a1f673cd230216112';
$currentPage = ( ( !isset($_GET["page"]) ) ? 0 : ($_GET["page"]-1) );
if($currentPage == -1 ) header("Location: ?page=1");
$perPage = 10;
$current = ($perPage * $currentPage);

$url = 'http://test.localfeedbackloop.com/api?apiKey=' . $apiKey . '&noOfReviews=' . $perPage . '&internal=1&yelp=1&google=1&offset=' . $current . '&threshold=1';
$json = file_get_contents($url);
$obj = json_decode($json);

$businessInfo = $obj->business_info;
$reviews = $obj->reviews;
$totalReviews = $businessInfo->total_rating->total_no_of_reviews;
$totalPages = round($totalReviews/$perPage);
?>
