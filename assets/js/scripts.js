function changePage(val) {
  window.location.href = ("?page=" + val);
}

function paginate(e) {
  e.preventDefault();
  var val = $(this).html();
  changePage(val);
}

function nextPrevBtnClicked(e) {
  e.preventDefault();
  var name = $(this).attr('id');
  var state = $(this).parent().hasClass('disabled');
  var current = parseInt( $('.current-page').html() );
  if(!state) changePage( (name == 'nextBtn') ? current+1 : current-1);
}

$( document ).ready(function() {
    $('.pagination li a.page').on('click', paginate);
    $('#nextBtn').on('click', nextPrevBtnClicked);
    $('#prevBtn').on('click', nextPrevBtnClicked);
});
